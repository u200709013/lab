package com.company;
public class FindPrimes {

    public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);

        //Print the numbers less than max
        //For each number less than max
        for (int number = 2; number < max; number++) {
            // Let divisor = 2;
            int divisor = 2;
            // let isPrime = true;
            boolean isPrime = true;
            // while divisor less than number
            while (divisor < number && isPrime) {
                // if the number is divisible by divisor
                if (number % divisor == 0)
                    isPrime = false; //isPrime = false
                //increment divisor
                divisor++;
            }
            // if the number is prime
            if (isPrime)
                System.out.println(number + " ");// print the number

        }
    }

}



