public class MyDateTime extends Object{

    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString(){
        return date + " " + time;

    }

    public void incrementDay() {
        date.incrementDay();

    }

    public void incrementHour() {
      incrementHour(1);

    }

    public void incrementHour(int diff) {
        int dayDiff =  time.incrementHour(diff);
        if(dayDiff < 0)
            date.decrementDay(-dayDiff);
        else
        date.incrementDay(dayDiff);
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);

    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);
        if(dayDiff < 0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);



    }

    public void decrementMinute(int diff) {
        incrementMinute(-30);

    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int i) {
        date.incrementDay(i);
    }

    public void decrementMonth(int i) {
        date.decrementMonth(2);

    }

    public void decrementDay(int i) {
        date.decrementDay(30);
    }

    public void incrementMonth(int i) {
        date.incrementMonth(16);
    }

    public void decrementYear(int i) {
        date.decrementYear(4);
    }

    public void incrementMonth() {
        date.incrementMonth(1);
    }

    public void incrementYear() {
        date.incrementYear(1);
    }

    public boolean isBefore(MyDateTime anotherDateTime) {

    }
}

