package drawing.version3;


import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;
import shapes.Shape;

public class TestDrawing {

	public static void main(String[] args) {

		Drawing drawing = new Drawing();

		drawing.addShape(new Circle(5));
		drawing.addShape(new Rectangle(5, 6));

		drawing.addShape(new Square(5));

		//drawing.addShape("Hello");

		//drawing.addShape(new Shape()) ;

		//drawing.addShape(new Integer(9);
		System.out.println("Total area = " + drawing.calculateTotalArea());

		drawing.addShape(new Circle(5));
		drawing.addShape(new Rectangle(5, 6));
		drawing.addShape(new Square(5));

		//drawing.addshape(new Shape());

		System.out.println("Total area = " + drawing.calculateTotalArea());
	}
}
